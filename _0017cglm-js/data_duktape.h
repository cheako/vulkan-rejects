#ifndef __DATA_DUKTAPE_H__
#define __DATA_DUKTAPE_H__
#include "data.h"

void update_vp();

void duktape_init();
void duktape_eval(const char *);
void duktape_update();
void duktape_exit();

void player_shoot();

void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mods);
void char_callback(GLFWwindow *window, unsigned int c);
void mouse_button_callback(GLFWwindow *window, int button, int action,
                           int mods);
void cursor_pos_callback(GLFWwindow *window, double xpos, double ypos);
void cursor_enter_callback(GLFWwindow *window, int entered);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

typedef struct {
  unsigned char forward;
  unsigned char backward;
  unsigned char turn_l;
  unsigned char turn_r;
  unsigned char climb_up;
  unsigned char climb_down;
} keys_count_t;
keys_count_t keys_count;

float center_dir;

typedef struct shared_ubo {
  mat4 vp;
  vec3 lightPos;
  float pada;
  vec3 viewPos;
  float padb;
} shared_ubo_t;

#endif /* __DATA_DUKTAPE_H__ */
