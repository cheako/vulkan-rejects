var glmCreate = require('cglm');
var glm = new glmCreate();

say(glm.add(2, 2));

Camera.direction = 0;

previous_frame = performance.now() / 1000;

Window.Keyboard.onKey = function (e) {
        if (e.action)
                switch (e.key) {
                        case Window.Keyboard.KEY_ESCAPE:
                                exit();
                                return true;
                        case Window.Keyboard.KEY_X:
                                say("Fire.");
                                return false;
                }
        return false;
};

Window.onUpdate = function () {
        var this_frame = performance.now() / 1000;
        var need_lookat = false;
        var t = Camera.position;

        var GET_KEY = function (key) {
                return Window.Keyboard.GetKey(key)
        };
        var GET_KEY2 = function (a, b) {
                return GET_KEY(a) || GET_KEY(b)
        };

        var forward, up;
        switch (((forward = GET_KEY2(Window.Keyboard.KEY_W, Window.Keyboard.KEY_UP)) ^ GET_KEY2(
                        Window.Keyboard.KEY_S, Window.Keyboard.KEY_DOWN)) ||
                (((up = GET_KEY(Window.Keyboard.KEY_R)) ^ GET_KEY(Window.Keyboard.KEY_V)) << 1)) {
                case 0:
                        break;
                case 1:
                        var dist = 1.5 * (forward ? this_frame - previous_frame :
                                previous_frame - this_frame);
                        t.x += Math.sin(Camera.direction) * dist;
                        t.z += Math.cos(Camera.direction) * dist;
                        need_lookat = true;
                        break;
                case 1 << 1:
                        t.y += 1.5 * (up ? this_frame -
                                previous_frame : previous_frame - this_frame);
                        need_lookat = true;
                        break;
                case 1 | (1 << 1):
                        var rate = 1.5 * 0.7071067811;
                        var dist = rate *
                                (forward ? this_frame -
                                        previous_frame :
                                        previous_frame - this_frame);
                        t.x += Math.sin(Camera.direction) * dist;
                        t.z += Math.cos(Camera.direction) * dist;
                        t.y += rate *
                                (up ? this_frame - previous_frame :
                                        previous_frame - this_frame);
                        need_lookat = true;
                        break;
                default:
                        say("Assert!");
        }

        var left;
        if ((left = GET_KEY2(Window.Keyboard.KEY_A, Window.Keyboard.KEY_LEFT)) ^
                GET_KEY2(Window.Keyboard.KEY_D, Window.Keyboard.KEY_RIGHT)) {
                Camera.direction += left ? this_frame - previous_frame :
                        previous_frame - this_frame;
                need_lookat = true;
        }

        if (need_lookat)
                Camera.position = t;
        previous_frame = this_frame;
};