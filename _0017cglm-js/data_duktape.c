#include "data_duktape.h"
#include "duk_module_duktape.h"
#include "whereami.h"
#include "data.h"
#include "../archive/cglm.min.h"
#include <assert.h>
#include <duktape.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/stat.h>
#include <regex.h>

void fatal_function(void *udata, const char *msg) {
  fprintf(stderr, "DukFatal: %s\n", msg);
  exit(1);
}

duk_context *ctx;
static inline void create_heap() {
  ctx = duk_create_heap(NULL, NULL, NULL, NULL, fatal_function);
  assert(ctx && "duk_create_heap_default: Failed.");
  duk_module_duktape_init(ctx);
}

static duk_ret_t native_say(duk_context *_ctx) {
  printf("%s\n", duk_to_string(_ctx, 0));
  return 0;
}

static duk_ret_t native_exit(duk_context *_ctx) {
  glfwSetWindowShouldClose(data_window, true);
  return 0;
}

static inline void global_functions() {
  duk_push_c_function(ctx, native_say, 1);
  duk_put_global_string(ctx, "say");
  duk_push_c_function(ctx, native_exit, 1);
  duk_put_global_string(ctx, "exit");
}

static inline duk_ret_t require_cglm(duk_context *_ctx) {
  duk_push_lstring(_ctx, (char*)___archive_cglm_min_js, ___archive_cglm_min_js_len);
  return 1;
}

regex_t regex;
static duk_ret_t native_require(duk_context *_ctx) {
    /* Nargs was given as 4 and we get the following stack arguments:
     *   index 0: id
     *   index 1: require
     *   index 2: exports
     *   index 3: module
     */

  const char *id = duk_to_string(_ctx, 0);
  if(0 == strcmp(id, "cglm")) {
    return require_cglm(_ctx);
  }

  do {
    int execlen;
    char *execpath = malloc((execlen = wai_getExecutablePath(NULL, 0, NULL)) + 1);
    wai_getExecutablePath(execpath, execlen, NULL);
    execpath[execlen] = '\0';
    bool idhasext;
    int reti = regexec(&regex, id, 0, NULL, 0);
    if (!reti) {
      idhasext = true;
    } else if (reti == REG_NOMATCH) {
      idhasext = false;
    } else {
      char msgbuf[100];
      regerror(reti, &regex, msgbuf, sizeof(msgbuf));
      fprintf(stderr, "Regex match failed: %s\n", msgbuf);
      exit(1);
    }
    char *folders[] = { "%s/lib/%s", "%s/%s", "%s/lib/%s.js", "%s/%s.js" };
    for(size_t t = 0; t < 4; t++) {
      size_t i = t % 2;
      struct stat sb;
      char trypath[PATH_MAX];
      sprintf(trypath, folders[i + (2 * !idhasext)], i != t ? "." : execpath, id);
      if (stat(trypath, &sb) == 0 && sb.st_size > 0) {
        char data[sb.st_size];
        FILE *f = fopen(trypath, "rb");
        if(!f) continue;
        fread(data, 1, sb.st_size, f);
        fclose(f);
        duk_push_lstring(_ctx, data, sb.st_size);
        return 1;
      }
    }
  } while(0);
  return duk_generic_error(ctx, "cannot find module: %s", id);
}

static inline void create_require() {
  /* Compile regular expression */
  if (regcomp(&regex, "\\.[jJ][sS]$", 0)) {
    fprintf(stderr, "Could not compile regex\n");
    exit(1);
  }
  duk_get_global_string(ctx, "Duktape");
  duk_push_c_function(ctx, native_require, 4);
  duk_put_prop_string(ctx, -2, "modSearch");
  duk_pop(ctx);
}

#define SETTER_ASSERT assert(duk_get_top(ctx) > 0);
float center_dir = 2;
static duk_ret_t getter_Camera_dir(duk_context *ctx) {
  duk_push_number(ctx, center_dir);
  return 1;
}

static duk_ret_t setter_Camera_dir(duk_context *ctx) {
  SETTER_ASSERT
  center_dir = duk_to_number(ctx, 0);
  update_vp();
  return 0;
}

#define SUBO data_model_shared_ubo
static duk_ret_t getter_Camera_pos(duk_context *ctx) {
  static const duk_uint_t flags = DUK_DEFPROP_HAVE_VALUE | DUK_DEFPROP_SET_WE;
  duk_push_object(ctx);
  duk_push_string(ctx, "x");
  duk_push_number(ctx, SUBO->viewPos[0]);
  duk_def_prop(ctx, -3, flags);
  duk_push_string(ctx, "y");
  duk_push_number(ctx, SUBO->viewPos[1]);
  duk_def_prop(ctx, -3, flags);
  duk_push_string(ctx, "z");
  duk_push_number(ctx, SUBO->viewPos[2]);
  duk_def_prop(ctx, -3, flags);
  return 1;
}

static duk_ret_t setter_Camera_pos(duk_context *ctx) {
  SETTER_ASSERT
  if (duk_is_null_or_undefined(ctx, 0) || !duk_is_object(ctx, 0))
    return 0;
  duk_get_prop_string(ctx, 0, "z");
  duk_get_prop_string(ctx, 0, "y");
  duk_get_prop_string(ctx, 0, "x");
  SUBO->viewPos[0] = duk_require_number(ctx, -1);
  SUBO->viewPos[1] = duk_require_number(ctx, -2);
  SUBO->viewPos[2] = duk_require_number(ctx, -3);
  update_vp();
  return 0;
}

static inline void create_Camera() {
  duk_push_bare_object(ctx);

  static const duk_uint_t flags =
      DUK_DEFPROP_HAVE_GETTER | DUK_DEFPROP_HAVE_SETTER |
      DUK_DEFPROP_SET_ENUMERABLE | DUK_DEFPROP_CLEAR_CONFIGURABLE;

  duk_push_string(ctx, "direction");
  duk_push_c_function(ctx, getter_Camera_dir, 0);
  duk_push_c_function(ctx, setter_Camera_dir, 1);
  duk_def_prop(ctx, -4, flags);

  duk_push_string(ctx, "position");
  duk_push_c_function(ctx, getter_Camera_pos, 0);
  duk_push_c_function(ctx, setter_Camera_pos, 1);
  duk_def_prop(ctx, -4, flags);

  duk_put_global_string(ctx, "Camera");
}

static duk_ret_t unsafe_onEvent(duk_context *ctx, void *ptr) {
  duk_call(ctx, 1);
  return 0;
}

static inline bool call_onEvent() {
  duk_idx_t top = duk_get_top(ctx);
  if (duk_safe_call(ctx, unsafe_onEvent, NULL, 1, 1)) {
    duk_get_prop_string(ctx, -1, "name");
    duk_get_prop_string(ctx, -2, "message");
    duk_get_prop_string(ctx, -3, "fileName");
    duk_get_prop_string(ctx, -4, "lineNumber");
    duk_get_prop_string(ctx, -5, "stack");
    fprintf(stderr, "Uncaught error in onEvent: %s: %s\n",
            duk_safe_to_string(ctx, -5), duk_safe_to_string(ctx, -4));
    fprintf(stderr, "              was at: %s line %s\n",
            duk_safe_to_string(ctx, -3), duk_safe_to_string(ctx, -2));
    fprintf(stderr, "         Stack trace: %s\n", duk_safe_to_string(ctx, -1));
    /* TODO: Mark these on the TextEditor. */
    duk_pop_n(ctx, 6);
  } else {
    bool ret = false;
    duk_idx_t diffrance = duk_get_top(ctx) - top;
    if (diffrance >= 0) {
      ret = duk_to_boolean(ctx, -1);
      duk_pop_n(ctx, diffrance + 1);
    } else
      assert(0 && "call_onEvent: Fatal, eat stack.");
    return ret;
  }
  return false;
}

#define GLFW_EVENT_HEAD(x)                                                     \
  static const duk_uint_t flags = DUK_DEFPROP_HAVE_VALUE | DUK_DEFPROP_SET_WE; \
  duk_push_object(ctx);                                                        \
  duk_push_string(ctx, "EventType");                                           \
  duk_push_string(ctx, #x);                                                    \
  duk_def_prop(ctx, -3, flags)
#define GLFW_INT_PARAM(x)                                                      \
  duk_push_string(ctx, #x);                                                    \
  duk_push_int(ctx, (x));                                                      \
  duk_def_prop(ctx, -3, flags)
#define GLFW_NUMBER_PARAM(x)                                                   \
  duk_push_string(ctx, #x);                                                    \
  duk_push_number(ctx, (x));                                                   \
  duk_def_prop(ctx, -3, flags)

void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mods) {
  duk_push_global_object(ctx);      /* -> [ global ] */
  duk_push_string(ctx, "Window");   /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window ] */
  duk_push_string(ctx, "Keyboard"); /* -> [ global Window "Keyboard" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window Keyboard ] */
  duk_push_string(ctx, "onKey");    /* -> [ global Window Keyboard "onKey" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window Keyboard val ] */
  if (duk_is_callable(ctx, -1)) {
    GLFW_EVENT_HEAD(Key);
    GLFW_INT_PARAM(key);
    GLFW_INT_PARAM(scancode);
    duk_push_string(ctx, "action");
    duk_push_boolean(ctx, action == GLFW_PRESS);
    duk_def_prop(ctx, -3, flags);
    GLFW_INT_PARAM(mods);
    if (!call_onEvent())
      ImGui_ImplGlfwVulkan_KeyCallback(window, key, scancode, action, mods);
  } else
    ImGui_ImplGlfwVulkan_KeyCallback(window, key, scancode, action, mods);
  duk_pop_n(ctx, 4); /* -> [  ] */
}

void char_callback(GLFWwindow *window, unsigned int c) {
  duk_push_global_object(ctx);    /* -> [ global ] */
  duk_push_string(ctx, "Window"); /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);          /* -> [ global Window ] */
  duk_push_string(ctx, "onChar"); /* -> [ global Window "onChar" ] */
  duk_get_prop(ctx, -2);          /* -> [ global Window val ] */
  if (duk_is_callable(ctx, -1)) {
    GLFW_EVENT_HEAD(Char);
    GLFW_INT_PARAM(c);
    duk_push_string(ctx, "char");
    duk_push_int(ctx, c);
    duk_def_prop(ctx, -3, flags);
    if (!call_onEvent())
      ImGui_ImplGlfwVulkan_CharCallback(window, c);
  } else
    ImGui_ImplGlfwVulkan_CharCallback(window, c);
  duk_pop_n(ctx, 3); /* -> [  ] */
}

void mouse_button_callback(GLFWwindow *window, int button, int action,
                           int mods) {
  duk_push_global_object(ctx);    /* -> [ global ] */
  duk_push_string(ctx, "Window"); /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);          /* -> [ global Window ] */
  duk_push_string(ctx,
                  "onMouseButton"); /* -> [ global Window "onMouseButton" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window val ] */
  if (duk_is_callable(ctx, -1)) {
    GLFW_EVENT_HEAD(MouseButton);
    GLFW_INT_PARAM(button);
    GLFW_INT_PARAM(action);
    GLFW_INT_PARAM(mods);
    if (!call_onEvent())
      ImGui_ImplGlfwVulkan_MouseButtonCallback(window, button, action, mods);
  } else
    ImGui_ImplGlfwVulkan_MouseButtonCallback(window, button, action, mods);
  duk_pop_n(ctx, 3); /* -> [  ] */
}

void cursor_pos_callback(GLFWwindow *window, double xpos, double ypos) {
  duk_push_global_object(ctx);         /* -> [ global ] */
  duk_push_string(ctx, "Window");      /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);               /* -> [ global Window ] */
  duk_push_string(ctx, "onCursorPos"); /* -> [ global Window "onCursorPos" ] */
  duk_get_prop(ctx, -2);               /* -> [ global Window val ] */
  if (duk_is_callable(ctx, -1)) {
    GLFW_EVENT_HEAD(CursorPos);
    GLFW_NUMBER_PARAM(xpos);
    GLFW_NUMBER_PARAM(ypos);
    call_onEvent();
  }
  duk_pop_n(ctx, 3); /* -> [  ] */
}

void cursor_enter_callback(GLFWwindow *window, int entered) {
  duk_push_global_object(ctx);    /* -> [ global ] */
  duk_push_string(ctx, "Window"); /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);          /* -> [ global Window ] */
  duk_push_string(ctx,
                  "onCursorEnter"); /* -> [ global Window "onCursorEnter" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window val ] */
  if (duk_is_callable(ctx, -1)) {
    GLFW_EVENT_HEAD(CursorPos);
    GLFW_INT_PARAM(entered);
    call_onEvent();
  }
  duk_pop_n(ctx, 3); /* -> [  ] */
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
  duk_push_global_object(ctx);      /* -> [ global ] */
  duk_push_string(ctx, "Window");   /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window ] */
  duk_push_string(ctx, "onScroll"); /* -> [ global Window "onScroll" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window val ] */
  if (duk_is_callable(ctx, -1)) {
    GLFW_EVENT_HEAD(MouseButton);
    GLFW_NUMBER_PARAM(xoffset);
    GLFW_NUMBER_PARAM(yoffset);
    if (!call_onEvent())
      ImGui_ImplGlfwVulkan_ScrollCallback(window, xoffset, yoffset);
  } else
    ImGui_ImplGlfwVulkan_ScrollCallback(window, xoffset, yoffset);
  duk_pop_n(ctx, 3); /* -> [  ] */
}

static duk_ret_t unsafe_onUpdate(duk_context *ctx, void *ptr) {
  duk_call(ctx, 0);
  return 0;
}

void duktape_update() {
  duk_push_global_object(ctx);      /* -> [ global ] */
  duk_push_string(ctx, "Window");   /* -> [ global "Window" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window ] */
  duk_push_string(ctx, "onUpdate"); /* -> [ global Window "onUpdate" ] */
  duk_get_prop(ctx, -2);            /* -> [ global Window val ] */
  if (duk_is_callable(ctx, -1)) {
    duk_idx_t top = duk_get_top(ctx);
    if (duk_safe_call(ctx, unsafe_onUpdate, NULL, 0, 1)) {
      duk_get_prop_string(ctx, -1, "name");
      duk_get_prop_string(ctx, -2, "message");
      duk_get_prop_string(ctx, -3, "fileName");
      duk_get_prop_string(ctx, -4, "lineNumber");
      duk_get_prop_string(ctx, -5, "stack");
      fprintf(stderr, "Uncaught error in onUpdate: %s: %s\n",
              duk_safe_to_string(ctx, -5), duk_safe_to_string(ctx, -4));
      fprintf(stderr, "              was at: %s line %s\n",
              duk_safe_to_string(ctx, -3), duk_safe_to_string(ctx, -2));
      fprintf(stderr, "         Stack trace: %s\n",
              duk_safe_to_string(ctx, -1));
      /* TODO: Mark these on the TextEditor. */
      duk_pop_n(ctx, 6);
    } else {
      duk_idx_t diffrance = duk_get_top(ctx) - top;
      if (diffrance >= 0) {
        duk_pop_n(ctx, diffrance + 1);
      } else
        assert(0 && "call_onUpdate: Fatal, eat stack.");
    }
  }
  duk_pop_n(ctx, 2); /* -> [  ] */
}

static const duk_uint_t empty_callback_flags =
    DUK_DEFPROP_SET_WRITABLE | DUK_DEFPROP_CLEAR_ENUMERABLE;
static inline void callbacks_Window() {
  duk_push_string(ctx, "onChar");
  duk_def_prop(ctx, -2, empty_callback_flags);
  duk_push_string(ctx, "onMouseButton");
  duk_def_prop(ctx, -2, empty_callback_flags);
  duk_push_string(ctx, "onCursorPos");
  duk_def_prop(ctx, -2, empty_callback_flags);
  duk_push_string(ctx, "onCursorEnter");
  duk_def_prop(ctx, -2, empty_callback_flags);
  duk_push_string(ctx, "onScroll");
  duk_def_prop(ctx, -2, empty_callback_flags);
  duk_push_string(ctx, "onUpdate");
  duk_def_prop(ctx, -2, empty_callback_flags);
}

static inline void create_Window_Keyboard_KEYs() {
  static const duk_uint_t flags = DUK_DEFPROP_HAVE_VALUE |
                                  DUK_DEFPROP_CLEAR_WRITABLE |
                                  DUK_DEFPROP_SET_ENUMERABLE;
#define DUK_GLFW_KEY(x)                                                        \
  duk_push_string(ctx, "KEY_" #x);                                             \
  duk_push_int(ctx, GLFW_KEY_##x);                                             \
  duk_def_prop(ctx, -3, flags)

  DUK_GLFW_KEY(UNKNOWN);
  DUK_GLFW_KEY(SPACE);
  DUK_GLFW_KEY(APOSTROPHE);
  DUK_GLFW_KEY(COMMA);
  DUK_GLFW_KEY(MINUS);
  DUK_GLFW_KEY(PERIOD);
  DUK_GLFW_KEY(SLASH);
  DUK_GLFW_KEY(0);
  DUK_GLFW_KEY(1);
  DUK_GLFW_KEY(2);
  DUK_GLFW_KEY(3);
  DUK_GLFW_KEY(4);
  DUK_GLFW_KEY(5);
  DUK_GLFW_KEY(6);
  DUK_GLFW_KEY(7);
  DUK_GLFW_KEY(8);
  DUK_GLFW_KEY(9);
  DUK_GLFW_KEY(SEMICOLON);
  DUK_GLFW_KEY(EQUAL);
  DUK_GLFW_KEY(A);
  DUK_GLFW_KEY(B);
  DUK_GLFW_KEY(C);
  DUK_GLFW_KEY(D);
  DUK_GLFW_KEY(E);
  DUK_GLFW_KEY(F);
  DUK_GLFW_KEY(G);
  DUK_GLFW_KEY(H);
  DUK_GLFW_KEY(I);
  DUK_GLFW_KEY(J);
  DUK_GLFW_KEY(K);
  DUK_GLFW_KEY(L);
  DUK_GLFW_KEY(M);
  DUK_GLFW_KEY(N);
  DUK_GLFW_KEY(O);
  DUK_GLFW_KEY(P);
  DUK_GLFW_KEY(Q);
  DUK_GLFW_KEY(R);
  DUK_GLFW_KEY(S);
  DUK_GLFW_KEY(T);
  DUK_GLFW_KEY(U);
  DUK_GLFW_KEY(V);
  DUK_GLFW_KEY(W);
  DUK_GLFW_KEY(X);
  DUK_GLFW_KEY(Y);
  DUK_GLFW_KEY(Z);
  DUK_GLFW_KEY(LEFT_BRACKET);
  DUK_GLFW_KEY(BACKSLASH);
  DUK_GLFW_KEY(RIGHT_BRACKET);
  DUK_GLFW_KEY(GRAVE_ACCENT);
  DUK_GLFW_KEY(WORLD_1);
  DUK_GLFW_KEY(WORLD_2);
  DUK_GLFW_KEY(ESCAPE);
  DUK_GLFW_KEY(ENTER);
  DUK_GLFW_KEY(TAB);
  DUK_GLFW_KEY(BACKSPACE);
  DUK_GLFW_KEY(INSERT);
  DUK_GLFW_KEY(DELETE);
  DUK_GLFW_KEY(RIGHT);
  DUK_GLFW_KEY(LEFT);
  DUK_GLFW_KEY(DOWN);
  DUK_GLFW_KEY(UP);
  DUK_GLFW_KEY(PAGE_UP);
  DUK_GLFW_KEY(PAGE_DOWN);
  DUK_GLFW_KEY(HOME);
  DUK_GLFW_KEY(END);
  DUK_GLFW_KEY(CAPS_LOCK);
  DUK_GLFW_KEY(SCROLL_LOCK);
  DUK_GLFW_KEY(NUM_LOCK);
  DUK_GLFW_KEY(PRINT_SCREEN);
  DUK_GLFW_KEY(PAUSE);
  DUK_GLFW_KEY(F1);
  DUK_GLFW_KEY(F2);
  DUK_GLFW_KEY(F3);
  DUK_GLFW_KEY(F4);
  DUK_GLFW_KEY(F5);
  DUK_GLFW_KEY(F6);
  DUK_GLFW_KEY(F7);
  DUK_GLFW_KEY(F8);
  DUK_GLFW_KEY(F9);
  DUK_GLFW_KEY(F10);
  DUK_GLFW_KEY(F11);
  DUK_GLFW_KEY(F12);
  DUK_GLFW_KEY(F13);
  DUK_GLFW_KEY(F14);
  DUK_GLFW_KEY(F15);
  DUK_GLFW_KEY(F16);
  DUK_GLFW_KEY(F17);
  DUK_GLFW_KEY(F18);
  DUK_GLFW_KEY(F19);
  DUK_GLFW_KEY(F20);
  DUK_GLFW_KEY(F21);
  DUK_GLFW_KEY(F22);
  DUK_GLFW_KEY(F23);
  DUK_GLFW_KEY(F24);
  DUK_GLFW_KEY(F25);
  DUK_GLFW_KEY(KP_0);
  DUK_GLFW_KEY(KP_1);
  DUK_GLFW_KEY(KP_2);
  DUK_GLFW_KEY(KP_3);
  DUK_GLFW_KEY(KP_4);
  DUK_GLFW_KEY(KP_5);
  DUK_GLFW_KEY(KP_6);
  DUK_GLFW_KEY(KP_7);
  DUK_GLFW_KEY(KP_8);
  DUK_GLFW_KEY(KP_9);
  DUK_GLFW_KEY(KP_DECIMAL);
  DUK_GLFW_KEY(KP_DIVIDE);
  DUK_GLFW_KEY(KP_MULTIPLY);
  DUK_GLFW_KEY(KP_SUBTRACT);
  DUK_GLFW_KEY(KP_ADD);
  DUK_GLFW_KEY(KP_ENTER);
  DUK_GLFW_KEY(KP_EQUAL);
  DUK_GLFW_KEY(LEFT_SHIFT);
  DUK_GLFW_KEY(LEFT_CONTROL);
  DUK_GLFW_KEY(LEFT_ALT);
  DUK_GLFW_KEY(LEFT_SUPER);
  DUK_GLFW_KEY(RIGHT_SHIFT);
  DUK_GLFW_KEY(RIGHT_CONTROL);
  DUK_GLFW_KEY(RIGHT_ALT);
  DUK_GLFW_KEY(RIGHT_SUPER);
  DUK_GLFW_KEY(MENU);
  DUK_GLFW_KEY(LAST);
#undef DUK_GLFW_KEY
}

static duk_ret_t Window_Keyboard_GetKey(duk_context *_ctx) {
  int key = duk_to_int(_ctx, 0);
  duk_push_boolean(_ctx, glfwGetKey(data_window, key) == GLFW_PRESS);
  return 1;
}

static inline void create_Window_Keyboard_functions() {
  static const duk_uint_t flags =
      DUK_DEFPROP_HAVE_VALUE | DUK_DEFPROP_CLEAR_WEC;
  duk_push_string(ctx, "GetKey");
  duk_push_c_function(ctx, Window_Keyboard_GetKey, 1);
  duk_def_prop(ctx, -3, flags);
  duk_push_string(ctx, "onKey");
  duk_def_prop(ctx, -2, empty_callback_flags);
}

static inline void create_Window_Keyboard() {
  duk_push_string(ctx, "Keyboard");
  duk_push_bare_object(ctx);

  create_Window_Keyboard_KEYs();
  create_Window_Keyboard_functions();

  duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE | DUK_DEFPROP_CLEAR_E);
}

static inline void create_Window() {
  duk_push_bare_object(ctx); /* -> [ Window ] */

  callbacks_Window();
  create_Window_Keyboard();

  duk_put_global_string(ctx, "Window"); /* -> [  ] */
}

static inline void setup_interfaces() {
  global_functions();
  create_require();
  create_Camera();
  create_Window();
}

void duktape_init() {
  create_heap();
  setup_interfaces();
}

static duk_ret_t unsafe_code(duk_context *ctx, void *src) {
  duk_eval_string(ctx, (char *)src);
  return 0;
}

void duktape_eval(const char *src) {
  duk_set_top(ctx, 0);
  if (duk_safe_call(ctx, unsafe_code, (void *)src, 0, 1)) {
    duk_get_prop_string(ctx, 0, "name");
    duk_get_prop_string(ctx, 0, "message");
    duk_get_prop_string(ctx, 0, "fileName");
    duk_get_prop_string(ctx, 0, "lineNumber");
    duk_get_prop_string(ctx, 0, "stack");
    fprintf(stderr, "Uncaught error in JS: %s: %s\n",
            duk_safe_to_string(ctx, 1), duk_safe_to_string(ctx, 2));
    fprintf(stderr, "              was at: %s line %s\n",
            duk_safe_to_string(ctx, 3), duk_safe_to_string(ctx, 4));
    fprintf(stderr, "         Stack trace: %s\n", duk_safe_to_string(ctx, 5));
    /* TODO: Mark these on the TextEditor. */
    duk_pop_n(ctx, 6);
  }
}

void duktape_exit() {
  duk_destroy_heap(ctx);
  ctx = NULL;
}
