#include "data_bulk.h"

#define MAX_FACES 4096

typedef struct
{
        vec3 position;
        vec3 normal;
        float uv[MAX_FACES][2];
        float boneWeights[MAX_BONES_PER_VERTEX];
        uint32_t boneIDs[MAX_BONES_PER_VERTEX];
} point_t;

typedef struct
{
        point_t *original_vertex[2], new_vertex[2];
        struct list_head list;
        struct list_head face_list[MAX_FACES];
        vertex_t edge_point;
        point_t midpoint;
} edge_t;

typedef struct
{
        struct list_head list;
        size_t faceid;
        struct list_head edges;
        vertex_t face_point;
} face_t;

typedef struct {
        size_t number_faces;
        struct list_head faces;
} model_t;

void subdivision_surface(size_t data_model_id, bool free_source, size_t count, void *geometry);
