#include "data.h"

// Maximum number of bones per mesh
// Must not be higher than same const in skinning shader
#define MAX_BONES 64

typedef struct
{
        mat4 model;
        mat4 bones[MAX_BONES];
        mat4 vp;
        vec3 lightPos;
        float pada;
        vec3 viewPos;
        float padb;
} material_0_ubo_t;

typedef struct
{
        vec3 position;
        vec3 normal;
        float uv[2];
        float boneWeights[MAX_BONES_PER_VERTEX];
        uint32_t boneIDs[MAX_BONES_PER_VERTEX];
} vertex_t;

void insert_triangle(vertex_t a, vertex_t b, vertex_t c);
