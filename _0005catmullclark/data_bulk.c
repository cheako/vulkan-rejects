#include "data_catmullclark.h"

/* Texture
 ####0AA1########
 ####AAAA########
 ####AAAA########
 ####2AA3########
 4BB56CC78DD9aEEb
 BBBBCCCCDDDDEEEE
 BBBBCCCCDDDDEEEE
 cBBdeCCfgDDhEEEi
 ####jFFk########
 ####FFFF########
 ####FFFF########
 ####lFFm########
 */

#define DMVPFR -0.5f
#define DMVPBA 0.5f
#define DMVPBO -0.5f
#define DMVPTO 0.5f
#define DMVPRI -0.5f
#define DMVPLE 0.5f

#define DMVUXONE 0.0f
#define DMVUXTWO 0.25f
#define DMVUXTHR 0.5f
#define DMVUXFOR 0.75f
#define DMVUXFIV 1.0f
#define DMVUYONE 0.0f
#define DMVUYTWO 1.0f / 3.0f
#define DMVUYTHR 2.0f / 3.0f
#define DMVUYFOR 1.0f

const point_t point0 = {
    {DMVPLE, DMVPTO, DMVPBA},
    {0.0f, DMVPTO * 2, 0.0f},
    {
        {DMVUXTWO, DMVUYONE},
    },
    {1.0f, 0.0f, 0.0f, 0.0f},
    {0, 0, 0, 0},
};

const point_t point1 = {
    {DMVPRI, DMVPTO, DMVPBA},
    {0.0f, DMVPTO * 2, 0.0f},
    {
        {DMVUXTHR, DMVUYONE},
    },
    {1.0f, 0.0f, 0.0f, 0.0f},
    {0, 0, 0, 0}};

const edge_t edge01 = {
    .original_vertex = {
        &point0, &point1},
    .list = {{&edge23.list[0], &edge02.list[0]}}};

const edge_t edge02 = {
    .original_vertex = {},
    .list = {
        {&edge01.list[0], &edge13.list[0]}}};

const edge_t edge13 = {
    .original_vertex = {},
    .list = {
        {&edge02.list[0], &edge23.list[0]}}};

const edge_t edge23 = {
    .original_vertex = {},
    .list = {
        {&edge13.list[0], &edge01.list[0]}}};

const face_t facea = {
    {&facef.list, &faceb.list}, 0, {&edge23.list[0], &edge01.list[0]}};

const model_t cube = {
        6, {&facef.list, &facea.list}
};

const vertex_t model_vertex[] = {
    {// 2
     {DMVPLE, DMVPTO, DMVPFR},
     {0.0f, DMVPTO * 2, 0.0f},
     {DMVUXTWO, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 3
     {DMVPRI, DMVPTO, DMVPFR},
     {0.0f, DMVPTO * 2, 0.0f},
     {DMVUXTHR, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 4
     {DMVPLE, DMVPTO, DMVPBA},
     {DMVPLE * 2, 0.0f, 0.0f},
     {DMVUXONE, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 5
     {DMVPLE, DMVPTO, DMVPFR},
     {DMVPLE * 2, 0.0f, 0.0f},
     {DMVUXTWO, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 6
     {DMVPLE, DMVPTO, DMVPFR},
     {0.0f, 0.0f, DMVPFR * 2},
     {DMVUXTWO, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 7
     {DMVPRI, DMVPTO, DMVPFR},
     {0.0f, 0.0f, DMVPFR * 2},
     {DMVUXTHR, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 8
     {DMVPRI, DMVPTO, DMVPFR},
     {DMVPRI * 2, 0.0f, 0.0f},
     {DMVUXTHR, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// 9
     {DMVPRI, DMVPTO, DMVPBA},
     {DMVPRI * 2, 0.0f, 0.0f},
     {DMVUXFOR, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// a
     {DMVPRI, DMVPTO, DMVPBA},
     {0.0f, 0.0f, DMVPBA * 2},
     {DMVUXFOR, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// b
     {DMVPLE, DMVPTO, DMVPBA},
     {0.0f, 0.0f, DMVPBA * 2},
     {DMVUXFIV, DMVUYTWO},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// c
     {DMVPLE, DMVPBO, DMVPBA},
     {DMVPLE * 2, 0.0f, 0.0f},
     {DMVUXONE, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// d
     {DMVPLE, DMVPBO, DMVPFR},
     {DMVPLE * 2, 0.0f, 0.0f},
     {DMVUXTWO, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// e
     {DMVPLE, DMVPBO, DMVPFR},
     {0.0f, 0.0f, DMVPFR * 2},
     {DMVUXTWO, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// f
     {DMVPRI, DMVPBO, DMVPFR},
     {0.0f, 0.0f, DMVPFR * 2},
     {DMVUXTHR, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// g
     {DMVPRI, DMVPBO, DMVPFR},
     {DMVPRI * 2, 0.0f, 0.0f},
     {DMVUXTHR, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// h
     {DMVPRI, DMVPBO, DMVPBA},
     {DMVPRI * 2, 0.0f, 0.0f},
     {DMVUXFOR, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// i
     {DMVPRI, DMVPBO, DMVPBA},
     {0.0f, 0.0f, DMVPBA * 2},
     {DMVUXFOR, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// j
     {DMVPLE, DMVPBO, DMVPBA},
     {0.0f, 0.0f, DMVPBA * 2},
     {DMVUXFIV, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// k
     {DMVPLE, DMVPBO, DMVPFR},
     {0.0f, DMVPBO * 2, 0.0f},
     {DMVUXTWO, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// k
     {DMVPRI, DMVPBO, DMVPFR},
     {0.0f, DMVPBO * 2, 0.0f},
     {DMVUXTHR, DMVUYTHR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// m
     {DMVPLE, DMVPBO, DMVPBA},
     {0.0f, DMVPBO * 2, 0.0f},
     {DMVUXTWO, DMVUYFOR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
    {// n
     {DMVPRI, DMVPBO, DMVPBA},
     {0.0f, DMVPBO * 2, 0.0f},
     {DMVUXTHR, DMVUYFOR},
     {1.0f, 0.0f, 0.0f, 0.0f},
     {0, 0, 0, 0}},
};
void *data_model_vertex = NULL;
VkDeviceSize data_model_vertex_size = 0;

static inline bool cmp_vertex(vertex_t x, vertex_t y)
{
        bool ret = (x.position == y.position) && (x.normal == y.normal) &&
                   (x.uv[0] == y.uv[0]) && (x.uv[1] == y.uv[1]);
        if (ret)
                for (size_t a = 0; a < 4; a++)
                {
                        size_t b;
                        for (b = 0; b < 4; b++)
                        {
                                if (x.boneIDs[a] == y.boneIDs[b])
                                {
                                        ret = ret && (x.boneWeights[a] == y.boneWeights[b]);
                                        break;
                                }
                                if (!ret)
                                        break;
                        }
                        if (b == 4)
                                ret = ret && (x.boneWeights[a] == 0.0f);
                        if (!ret)
                                break;
                }
        return ret;
}

inline static uint32_t get_vertex_id(vertex_t v)
{
        for (vertex_t *p = data_model_vertex; p < (uintptr_t)data_model_vertex + data_model_vertex_size; p++)
        {
                if (cmp_vertex(*p, v))
                        return ((uintptr_t)p - (uintptr_t)data_model_vertex) / sizeof(v);
        }
        uint32_t ret = data_model_vertex_size / sizeof(v);
        data_model_vertex_size += sizeof(v);
        if (data_model_vertex != NULL)
        {
                void *n = NULL;
                while (n == NULL)
                        n = realloc(data_model_vertex, data_model_vertex_size);
                data_model_vertex = n;
        }
        else
                while (data_model_vertex == NULL)
                        data_model_vertex = malloc(data_model_vertex_size);
        {
                vertex_t *p = (vertex_t *)((intptr_t)data_model_vertex + ret * sizeof(v));
                *p = v;
        }
        return ret;
}

void insert_triangle(vertex_t a, vertex_t b, vertex_t c)
{
        uint32_t *ptr, aid, bid, cid;
        aid = get_vertex_id(a);
        bid = get_vertex_id(b);
        cid = get_vertex_id(c);
        data_model_index_size += sizeof(uint32_t) * 3;
        if (data_model_index != NULL)
        {
                void *n = NULL;
                while (n == NULL)
                        n = realloc(data_model_index, data_model_index_size);
                data_model_index = n;
        }
        else
                while (data_model_index == NULL)
                        data_model_index = malloc(data_model_index_size);
        ptr = (uint32_t *)((uintptr_t)data_model_index + data_model_index_size - sizeof(uint32_t) * 3);
        ptr[0] = aid;
        ptr[1] = bid;
        ptr[2] = cid;
}

void data_load_models()
{
}

const VkVertexInputBindingDescription data_binding_description = {
    .binding = 0,
    .stride = sizeof(vertex_t),
    .inputRate = VK_VERTEX_INPUT_RATE_VERTEX};
const VkVertexInputAttributeDescription data_attribute_descriptions[] = {
    {.location = 0,
     .binding = 0,
     .format = VK_FORMAT_R32G32B32_SFLOAT,
     .offset = offsetof(vertex_t, position)},
    {.location = 1,
     .binding = 0,
     .format = VK_FORMAT_R32G32B32_SFLOAT,
     .offset = offsetof(vertex_t, normal)},
    {.location = 2,
     .binding = 0,
     .format = VK_FORMAT_R32G32_SFLOAT,
     .offset = offsetof(vertex_t, uv)},
    {.location = 3,
     .binding = 0,
     .format = VK_FORMAT_R32G32B32A32_SFLOAT,
     .offset = offsetof(vertex_t, boneWeights)},
    {.location = 4,
     .binding = 0,
     .format = VK_FORMAT_R32G32B32A32_SINT,
     .offset = offsetof(vertex_t, boneIDs)}};
const uint32_t data_attribute_descriptions_count =
    sizeof(data_attribute_descriptions) /
    sizeof(data_attribute_descriptions[0]);

struct
{
        uint32_t first, second, third;
} *data_model_index = NULL;
VkDeviceSize data_model_index_size = 0;

data_model_t data_models[] = {
    {
        "Cube",
        0,
        0,
        0,
    },
};

const data_materials_t data_materials[] = {
    {
        "Example",
        0,
        sizeof(material_0_ubo_t),
    },
};
